#!/bin/bash

cd android
source build/envsetup.sh
lunch 16
make -j4 clean
rm -f device/softwinner/wing-a70xh/kernel
rm -rf device/softwinner/wing-a70xh/modules
cd ..

for i in $(find lichee/ -maxdepth 1 -type d); do
  if [ -f $i/Makefile ]; then
    make -C $i ARCH=arm CROSS_COMPILE=arm-linux-androideabi- distclean
  fi
done

echo "Removing output"
for i in $(find lichee/linux-*/output -maxdepth 0 -type d); do
  rm -vrf $i;
done

echo "Removing Kernels"
for i in $(find lichee/linux-* -maxdepth 1 -name bImage -type f); do
  rm -vf $i;
done

echo "Removing Images"
for i in $(find lichee/tools/pack -maxdepth 1 -name *.img -type f); do
  rm -vf $i;
done

echo "Removing out"
rm -vrf lichee/out/*
rm -vrf lichee/tools/pack/out/*

