#!/bin/bash

cd lichee
./build.sh -p sun7i_android -k 3.4
cd ../android
source build/envsetup.sh
lunch 16
extract-bsp
make -j4
pack
cd ..
